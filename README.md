SCRL Lock Mic Mute
By Lucas Beeres 2017

To install, run the installation executable. It will install everything where it's supposed to be. (Note: The installer has no GUI.)

Alternatively, if you don't trust the preencoded .exe files, you can use the .ahk to .exe converter on the main script and installer .ahk files.
Note that the main script needs to be called "SCRL Lock Mic Mute.exe", unless all references in the installer code is modified to match.

Credits for the sound card analysis go to Peter Provost. (https://github.com/PProvost/AutoHotKey)
The icons are a modified version of an original by the user "Madebyoliver" (http://www.flaticon.com/authors/madebyoliver)